module.exports = {
  'extends': 'airbnb',
  'parser': 'babel-eslint',
  'parserOptions': {
    'ecmaFeatures': {
      'jsx': true,
      'experimentalObjectRestSpread': true
      //'impliedStrict': true // TODO Babel runs on strict mode, so strict mode should not be needed on client folder ( still needed on server )
    }
  },
  'globals': {
    'HttpError': true,
    'PG': true,
    'sequelize': true,
    '_': true,
    '__ENV__': true,
    'api': true,
    'window': true,
    'document': true,
    /* Chai global variables*/
    'expect': true,
    'it': true,
    'describe': true,
    'before': true,
    'Constants': true,
  },
  'settings': {
    'import/resolver': {
      'webpack': {
        'config': 'webpack.config.js',
      },
    },
  },
  'rules': {
    'strict': 0,
    'one-var': 0,
    'react/jsx-one-expression-per-line': 0,
    'no-underscore-dangle': 0,
    'no-param-reassign': ['error', { 'props': false }],
    'no-shadow': 0,
    'arrow-body-style': 0,
    'comma-dangle': ['error', {
      'arrays': 'always-multiline',
      'objects': 'always-multiline',
      'imports': 'always-multiline',
      'exports': 'always-multiline',
    }],
    'class-methods-use-this': 0,
    'consistent-return': 0,
    'no-use-before-define': ['error', { 'functions': false }],
    'new-cap': 0,
    'no-restricted-syntax': ['error', 'WithStatement'],
    'key-spacing': ['error', { 'mode': 'minimum' }],
    'import/no-unresolved': 0,
    'import/extensions': 0,
    'import/no-extraneous-dependencies': 0,
    'import/prefer-default-export': 0,
    'import/no-named-as-default': 0,
    'import/no-named-as-default-member': 0,
    'implicit-arrow-linebreak': 0,
    'max-len': 0,
    'no-param-reassign': 0,
    'no-console': 0,
    'space-before-function-paren': 0,
    'func-names': 0,
    'newline-per-chained-call': 0,
    'react/jsx-filename-extension': [1, { 'extensions': ['.js', '.jsx'] }],
    'react/no-array-index-key': 0,
    'react/forbid-prop-types': 0,
    'react/require-default-props': 0,
    'react/no-unused-prop-types': 1,
    'function-paren-newline': 0,
    'object-curly-newline': 0,
    'no-plusplus': ['error', { 'allowForLoopAfterthoughts': true }],
    'jsx-a11y/anchor-is-valid': ['error', {
      'components': ['Link'],
      'specialLink': ['to'],
    }],
    'jsx-a11y/click-events-have-key-events': 0,
    'react/jsx-curly-brace-presence': 0,
    'jsx-a11y/label-has-for': [2, {
      'required': {
        'some': ['nesting', 'id'],
      },
    }],
    'linebreak-style': 0,
    'global-require': 0,
    'import/no-dynamic-require': 0,
    'import/no-webpack-loader-syntax': 0,
    'react/jsx-wrap-multilines': 0
  },
  'plugins': [
    'react',
  ],
  'env': {
    'node': true,
    'browser': true,
    'jest': true,
  }
};
