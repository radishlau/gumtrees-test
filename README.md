# Gumtree Test

## Run the project

## For the first time

- `brew install node`
- `npm install`

## Development

- `npm run start` - Create WebpackDev server with hot loader
- `npm run test` - Run component tests
- `npm run storybook`- Storybook available on [http://localhost:6006](http://localhost:6006/)

## Production

- `npm run production`