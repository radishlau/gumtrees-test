import React from 'react';
import PropTypes from 'prop-types';
import { pure } from 'recompose';
import classNames from 'classnames';

import './style.scss';

const Tag = (props) => {
  const {
    color,
    label,
    labelColor,
    handleClick,
    theme,
    ...others
  } = props;

  return (
    <div
      className={classNames('tag', theme)}
      {...others}
    >
      {label}
    </div>
  );
};

Tag.defaultProps = {
  theme: 'primary',
};

Tag.propTypes = {
  theme: PropTypes.oneOf(['primary', 'grey', 'warn', 'danger', 'select']),
  label: PropTypes.string.isRequired,
  color: PropTypes.string,
  labelColor: PropTypes.string,
  handleClick: PropTypes.func,
};

export default pure(Tag);
