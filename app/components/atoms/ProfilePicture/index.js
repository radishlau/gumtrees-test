import React from 'react';
import PropTypes from 'prop-types';
import { withState } from 'recompose';

import fallbackSrc from '../../../assets/images/profile-placeholder.png';
import './style.scss';

const ProfilePicture = ({ imageUrl, name, isImageBroken, setIsImageBroken }) => (
  <div className="profile-picture">
    <img
      src={(isImageBroken || !imageUrl) ? fallbackSrc : imageUrl}
      onError={() => setIsImageBroken(true)}
      alt={name}
    />
  </div>
);

ProfilePicture.propTypes = {
  imageUrl: PropTypes.string,
  name: PropTypes.string,
  isImageBroken: PropTypes.bool,
  setIsImageBroken: PropTypes.func,
};

export default withState('isImageBroken', 'setIsImageBroken', false)(ProfilePicture);
