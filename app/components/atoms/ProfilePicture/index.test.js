import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import ProfilePicture from '.';

configure({ adapter: new Adapter() });

describe('ProfilePicture Component', () => {
  it('should exist', () => {
    const wrapper = mount(<ProfilePicture />);
    expect(wrapper).toBeTruthy();
  });

  it('should render 1 image', () => {
    const wrapper = mount(<ProfilePicture />);
    expect(wrapper.find('img')).toHaveLength(1);
  });
});
