import React from 'react';
import { storiesOf } from '@storybook/react';
import ProfilePicture from '.';

storiesOf('ProfilePicture', module)
  .add('default', () => (
    <ProfilePicture imageUrl="https://s3.amazonaws.com/cms-assets.tutsplus.com/uploads/users/810/profiles/19338/profileImage/profile-square-extra-small.png" />
  ))
  .add('fallback', () => (
    <ProfilePicture imageUrl="invalid_link" />
  ));
