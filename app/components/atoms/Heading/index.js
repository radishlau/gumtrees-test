import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { pure } from 'recompose';

import './style.scss';

const Heading = ({ className, level, children, inline, theme, light, medium, weightLight, weightBold, noMargin, ...props }) =>
  React.createElement(
    `h${level}`,
    Object.assign(
      {},
      props,
      {
        className: classNames(
          className,
          'heading',
          `heading-${level}`,
          {
            medium,
            light,
            theme,
            inline,
          },
          noMargin && 'no-margin',
          weightLight && 'weight-light',
          weightBold && 'weight-bold'
        ),
      }
    ),
    children
  );

Heading.defaultProps = {
  className: '',
  level: 1,
};

Heading.propTypes = {
  className: PropTypes.string,
  level: PropTypes.number,
  children: PropTypes.node,
  theme: PropTypes.bool,
  inline: PropTypes.bool,
  medium: PropTypes.bool,
  light: PropTypes.bool,
  noMargin: PropTypes.bool,
  weightLight: PropTypes.bool,
  weightBold: PropTypes.bool,
};

export default pure(Heading);
