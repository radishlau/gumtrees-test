import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './style.scss';

const P = ({
  children,
  className,
  small,
  large,
  light,
  medium,
  semiBold,
  bold,
  inline,
  upper,
  noMargin,
  error,
  center,
  theme,
}) => (
  <p
    className={classNames(
      'paragraph',
      className,
      {
        small,
        large,
        upper,
        inline,
        light,
        medium,
        center,
        error,
        theme,
      },
      noMargin && 'no-margin',
      semiBold && 'semi-bold',
      bold && 'bold'
    )}
  >
    {children}
  </p>
);

P.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  className: PropTypes.string,
  small: PropTypes.bool,
  large: PropTypes.bool,
  light: PropTypes.bool,
  medium: PropTypes.bool,
  semiBold: PropTypes.bool,
  bold: PropTypes.bool,
  upper: PropTypes.bool,
  inline: PropTypes.bool,
  noMargin: PropTypes.bool,
  center: PropTypes.bool,
  error: PropTypes.bool,
  theme: PropTypes.bool,
};

export default P;
