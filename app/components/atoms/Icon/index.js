import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './style.scss';

const Icon = ({ name, large, xlarge }) => {
  const svg = require(`./icons/icon_${name}.svg`);
  return (
    <img
      className={classNames('icon', { large, xlarge })}
      src={svg}
      alt={name}
    />
  );
};

Icon.propTypes = {
  name: PropTypes.string.isRequired,
  large: PropTypes.bool,
  xlarge: PropTypes.bool,
};

export default Icon;
