import React from 'react';
import { storiesOf } from '@storybook/react';
import Icon from '.';

const renderIcons = props => (
  <div>
    <p>arrow_left: <Icon name="arrow_left" {...props} /></p>
    <p>arrow_right: <Icon name="arrow_right" {...props} /></p>
    <p>bullet: <Icon name="bullet" {...props} /></p>
    <p>education: <Icon name="education" {...props} /></p>
    <p>email: <Icon name="email" {...props} /></p>
    <p>gumtree: <Icon name="gumtree" {...props} /></p>
    <p>location: <Icon name="location" {...props} /></p>
    <p>phone: <Icon name="phone" {...props} /></p>
  </div>
);

storiesOf('Icon', module)
  .add('default', () => renderIcons());
