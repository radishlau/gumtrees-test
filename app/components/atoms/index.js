import Heading from './Heading';
import Icon from './Icon';
import P from './P';
import ProfilePicture from './ProfilePicture';
import Tag from './Tag';

export {
  Heading,
  Icon,
  P,
  ProfilePicture,
  Tag,
};
