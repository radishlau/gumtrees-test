import Content from './Content';
import Page from './Page';

export {
  Content,
  Page,
};
