import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './style.scss';

const Content = ({
  children,
  narrow,
  medium,
  contentClassName,
  noPadding,
  ...props
}) => (
  <div
    className={classNames(
      'page__content',
      {
        narrow,
        medium,
        [contentClassName]: !!contentClassName,
      },
      noPadding && 'no-padding',
    )}
    {...props}
  >
    {children}
  </div>
);

Content.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  narrow: PropTypes.bool,
  medium: PropTypes.bool,
  contentClassName: PropTypes.string,
  noPadding: PropTypes.bool,
};

export default Content;
