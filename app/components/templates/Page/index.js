import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './style.scss';

const Wrapper = ({ children, className }) => (
  <div
    className={classNames(
      'page__wrapper stretch',
      className
    )}
  >
    {children}
  </div>
);

Wrapper.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  className: PropTypes.string,
};

const Page = ({ footer, children, ...props }) => {
  return (
    <Wrapper {...props}>
      <div className="content stretch">{children}</div>
      <footer>{footer}</footer>
    </Wrapper>
  );
};

Page.propTypes = {
  footer: PropTypes.node,
  children: PropTypes.any.isRequired,
  contentClassName: PropTypes.string,
  narrow: PropTypes.bool,
};

export default Page;
