import React from 'react';
import PropTypes from 'prop-types';

import { Icon } from '../../atoms';
import {
  CandidateCardHeader,
  PersonalDetails,
  Experiences,
  Skills,
  Languages,
} from '../../molecules';

import './style.scss';

const CandidateCard = ({ data = {} }) => (
  <div className="candidate-card">
    <CandidateCardHeader {...data} />
    <div className="details">
      <PersonalDetails {...data} />
      <Experiences data={data.workExperiences} />
      <Skills {...data} />
      <Languages {...data} />
    </div>
    <Icon name="gumtree" large />
  </div>
);

CandidateCard.propTypes = {
  data: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    profilePicture: PropTypes.string,
    biography: PropTypes.string,
    phone: PropTypes.string,
    email: PropTypes.string,
    basedLocation: PropTypes.string,
    educationLevel: PropTypes.string,
    workExperiences: PropTypes.array,
    skills: PropTypes.array,
    languages: PropTypes.array,
  }),
};

export default CandidateCard;
