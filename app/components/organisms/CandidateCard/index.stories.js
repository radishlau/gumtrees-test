import React from 'react';
import { storiesOf } from '@storybook/react';
import CandidateCard from '.';

import data from '../../../data/candidates.json';

storiesOf('CandidateCard', module)
  .add('default', () => (
    <CandidateCard data={data[0]} />
  ));
