import React from 'react';
import PropTypes from 'prop-types';

import { Heading } from '../../atoms';
import IconWithText from '../IconWithText';
import './style.scss';

const PersonalDetails = ({
  phone,
  email,
  basedLocation,
  educationLevel,
}) =>
  (
    <div className="personal-details">
      <Heading level={4}>Personal Details</Heading>
      <div className="items">
        <IconWithText icon="phone" text={phone} />
        <IconWithText icon="email" text={email} />
        <IconWithText icon="location" text={basedLocation} />
        <IconWithText icon="education" text={educationLevel} />
      </div>
    </div>
  );

PersonalDetails.propTypes = {
  phone: PropTypes.string,
  email: PropTypes.string,
  basedLocation: PropTypes.string,
  educationLevel: PropTypes.string,
};

export default PersonalDetails;
