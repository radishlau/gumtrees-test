import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, number } from '@storybook/addon-knobs';
import PersonalDetails from '.';

storiesOf('PersonalDetails', module)
  .addDecorator(withKnobs)
  .add('interactive', () => (
    <PersonalDetails
      phone={number('Phone', 95210678)}
      basedLocation={text('Location', 'Hong Kong')}
      email={text('E-mail', 'Rachel')}
      educationLevel={text('Education Level', 'Bachelor')}
    />
  ))
  .add('default', () => (
    <PersonalDetails
      phone="95210611"
      email="pakwlau@gmail.com"
      basedLocation="Hong Kong"
      educationLevel="Master"
    />
  ))
  .add('with missing details', () => (
    <PersonalDetails
      phone="95210611"
      email=""
      basedLocation=""
    />
  ));
