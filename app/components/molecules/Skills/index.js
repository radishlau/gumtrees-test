import React from 'react';
import PropTypes from 'prop-types';

import TagSection from '../TagSection';

const Skills = ({ skills }) => (
  <TagSection title="Skills" tags={skills} />
);

Skills.propTypes = {
  skills: PropTypes.arrayOf(PropTypes.string),
};

export default Skills;
