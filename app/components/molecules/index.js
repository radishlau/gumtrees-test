import CandidateCardHeader from './CandidateCardHeader';
import CandidatePageTitle from './CandidatePageTitle';
import Experiences from './Experiences';
import IconWithText from './IconWithText';
import Languages from './Languages';
import PageNavigationButton from './PageNavigationButton';
import PersonalDetails from './PersonalDetails';
import Skills from './Skills';
import TagSection from './TagSection';

export {
  CandidateCardHeader,
  CandidatePageTitle,
  Experiences,
  IconWithText,
  Languages,
  PageNavigationButton,
  PersonalDetails,
  Skills,
  TagSection,
};
