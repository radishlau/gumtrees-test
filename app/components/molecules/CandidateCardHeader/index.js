import React from 'react';
import PropTypes from 'prop-types';

import { P, Heading, ProfilePicture } from '../../atoms';
import './style.scss';

const CandidateCardHeader = ({ firstName, lastName, profilePicture, biography }) => {
  const fullName = `${firstName} ${lastName}`;
  return (
    <div>
      <span id="background-pattern" />
      <div className="candidate-card-header">
        <Heading noMargin theme level={2}> {fullName}</Heading>
        <ProfilePicture
          imageUrl={profilePicture}
          name={fullName}
        />
        <P large medium className="biography">{biography}</P>
      </div>
    </div>
  );
};

CandidateCardHeader.propTypes = {
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  biography: PropTypes.string,
  profilePicture: PropTypes.string,
};

export default CandidateCardHeader;
