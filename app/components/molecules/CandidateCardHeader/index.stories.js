import React from 'react';
import { storiesOf } from '@storybook/react';
import CandidateCardHeader from '.';

storiesOf('CandidateCardHeader', module)
  .add('default', () => (
    <CandidateCardHeader
      firstName="Nick"
      lastName="Jones"
      profilePicture="https://s3.amazonaws.com/cms-assets.tutsplus.com/uploads/users/810/profiles/19338/profileImage/profile-square-extra-small.png"
      biography="I have worked for many famous restaurants in Sydney. I love to bring happiness to my customers."
    />
  ));
