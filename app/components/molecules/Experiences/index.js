import React from 'react';
import PropTypes from 'prop-types';
import { pure } from 'recompose';

import { Heading, P, Icon } from '../../atoms';

import { getDurationText } from './utils';
import './style.scss';

const Experience = ({
  position,
  place,
  description,
  fromDate,
  toDate,
}) =>
  (
    <div className="experience">
      <div className="position">
        <Icon name="bullet" />
        <Heading level={5} theme noMargin inline>{position}</Heading>
      </div>
      <P>{place}</P>
      <P>{getDurationText(fromDate, toDate)}</P>
      <P light>{description}</P>
    </div>
  );

Experience.propTypes = {
  position: PropTypes.string,
  place: PropTypes.string,
  description: PropTypes.string,
  fromDate: PropTypes.string,
  toDate: PropTypes.string,
};

const Experiences = ({ data = [] }) => {
  const shouldShow = data.length > 0;
  return shouldShow && (
    <div>
      <Heading level={4}>Experience</Heading>
      {data.map((data, i) => <Experience key={i} {...data} />)}
    </div>
  );
};

Experiences.propTypes = {
  data: PropTypes.array,
};

export default pure(Experiences);
