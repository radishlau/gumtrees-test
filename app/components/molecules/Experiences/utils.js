import moment from 'moment';

const isEmptyText = v => !!v;

const numberToTextWithUnit = (number, unit) => {
  if (number === 0) return '';
  if (number > 0) {
    return `${number} ${unit}${number > 1 ? 's' : ''}`;
  }
  return '';
};

const joinTextArrayWithSpace = array => `${array.filter(isEmptyText).join(' ')}`;

export const getDurationText = (fromDate, toDate) => {
  const fromMoment = moment(fromDate);
  // if toDate is undefined, it returns today
  const toMoment = moment(toDate);
  const differenceInYears = toMoment.diff(fromMoment, 'years');
  const differenceInMonths = toMoment.diff(fromMoment, 'months');

  const displayMonthCount = differenceInYears > 0 && differenceInMonths >= 12
    ? (differenceInMonths - differenceInYears * 12)
    : differenceInMonths;

  const yearText = numberToTextWithUnit(differenceInYears, 'yr');
  const monthText = numberToTextWithUnit(displayMonthCount, 'mo');

  const durationText = joinTextArrayWithSpace([yearText, monthText]);

  const fromDateText = fromMoment.format('MMM YYYY');
  const toDateText = toMoment.format('MMM YYYY');

  const isCurrentJob = !toDate;
  const datesText = `${fromDateText} - ${isCurrentJob ? 'Current' : toDateText}`;

  return joinTextArrayWithSpace([datesText, durationText && `(${durationText})`]);
};
