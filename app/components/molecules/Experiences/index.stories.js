import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, object } from '@storybook/addon-knobs';
import Experiences from '.';

const json = [
  {
    position: 'Senior Waiter',
    place: 'Hilton Hotel',
    description: 'I work as a senior waiter serving the French restuarant inside',
    fromDate: '2015-03-01',
  },
  {
    position: 'Sandwich Hand',
    place: 'Subway',
    description: 'It was my part time job during my study in university',
    fromDate: '2012-01-01',
    toDate: '2015-03-01',
  },
];

storiesOf('Experiences', module)
  .addDecorator(withKnobs)
  .add('interactive', () => (
    <Experiences
      data={object('Data', json)}
    />
  ));
