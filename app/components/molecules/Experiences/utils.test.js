import {
  getDurationText,
} from './utils';

describe('Duration', () => {
  test('less than a month ', () => {
    const fromDate = '2015-03-01';
    const toDate = '2015-03-15';
    const duration = 'Mar 2015 - Mar 2015';

    expect(getDurationText(fromDate, toDate)).toBe(duration);
  });

  test('one month', () => {
    const fromDate = '2015-03-01';
    const toDate = '2015-04-15';
    const duration = 'Mar 2015 - Apr 2015 (1 mo)';

    expect(getDurationText(fromDate, toDate)).toBe(duration);
  });

  test('one year ', () => {
    const fromDate = '2015-03-01';
    const toDate = '2016-03-01';
    const duration = 'Mar 2015 - Mar 2016 (1 yr)';

    expect(getDurationText(fromDate, toDate)).toBe(duration);
  });

  test('3 years and 2months', () => {
    const fromDate = '2012-01-01';
    const toDate = '2015-03-01';
    const duration = 'Jan 2012 - Mar 2015 (3 yrs 2 mos)';

    expect(getDurationText(fromDate, toDate)).toBe(duration);
  });
});
