import React from 'react';
import PropTypes from 'prop-types';

import TagSection from '../TagSection';

const Languages = ({ languages }) => (
  <TagSection title="Languages" tags={languages} />
);

Languages.propTypes = {
  languages: PropTypes.arrayOf(PropTypes.string),
};

export default Languages;
