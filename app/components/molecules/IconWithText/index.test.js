import React from 'react';
import { shallow, mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import IconWithText from '.';

configure({ adapter: new Adapter() });

describe('IconWithText Component', () => {
  it('should exist', () => {
    const wrapper = shallow(<IconWithText icon="arrow_left" />);
    expect(wrapper).toBeTruthy();
  });

  it('should show "-" when no text is provided', () => {
    const wrapper = mount(<IconWithText icon="arrow_left" />);
    expect(wrapper.find('div').text()).toBe('-');
  });
});
