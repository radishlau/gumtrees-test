import React from 'react';
import PropTypes from 'prop-types';

import { Icon, P } from '../../atoms';

import './style.scss';

const IconWithText = ({ icon, text }) => (
  <div className="icon-with-text">
    <Icon name={icon} />
    <P noMargin medium>{text || '-'}</P>
  </div>
);

IconWithText.propTypes = {
  icon: PropTypes.string.isRequired,
  text: PropTypes.string,
};

export default IconWithText;
