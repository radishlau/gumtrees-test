import React from 'react';
import { storiesOf } from '@storybook/react';
import PageNavigationButton from '.';

storiesOf('PageNavigationButton', module)
  .add('previous', () => (
    <PageNavigationButton />
  ))
  .add('next', () => (
    <PageNavigationButton next />
  ));
