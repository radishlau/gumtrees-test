import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { Icon } from '../../atoms';
import './style.scss';

const PageNavigationButton = ({ next, onClick }) => (
  <div
    className={classNames('page-navigation-button', { next })}
    role="button"
    tabIndex={next ? 1 : 0}
    onClick={onClick}
  >
    <Icon name={`arrow_${next ? 'right' : 'left'}`} xlarge />
  </div>
);

PageNavigationButton.propTypes = {
  next: PropTypes.bool,
  onClick: PropTypes.func,
};

export default PageNavigationButton;
