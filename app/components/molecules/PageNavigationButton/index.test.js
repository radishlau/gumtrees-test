import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import PageNavigationButton from '.';

configure({ adapter: new Adapter() });

describe('PageNavigationButton Component', () => {
  let wrapper;
  const mockClickFn = jest.fn();

  beforeEach(() => {
    wrapper = shallow(<PageNavigationButton onClick={mockClickFn} />);
  });

  it('should exist', () => {
    expect(wrapper).toBeTruthy();
  });

  it('calls onClick when clicked', () => {
    wrapper.find('div').simulate('click');
    expect(mockClickFn.mock.calls.length).toBe(1);
  });
});
