import React from 'react';
import PropTypes from 'prop-types';
import { pure } from 'recompose';

import { Heading, Tag } from '../../atoms';
import './style.scss';

const TagSection = ({ title, tags = [] }) => {
  const shouldShow = tags.length > 0;
  return shouldShow && (
    <div className="tag-section">
      <Heading level={4}>{title}</Heading>
      {tags.map(tag => <Tag label={tag} key={tag} />)}
    </div>
  );
};

TagSection.propTypes = {
  title: PropTypes.string,
  tags: PropTypes.array,
};

export default pure(TagSection);
