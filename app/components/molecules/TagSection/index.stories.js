import React from 'react';
import { storiesOf } from '@storybook/react';
import TagSection from '.';

storiesOf('TagSection', module)
  .add('default', () => (
    <TagSection
      title="Title"
      tags={[
        'English',
        'Madarin',
        'French',
      ]}
    />
  ))
  .add('empty', () => (
    <TagSection
      title="Title"
    />
  ));
