import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const CandidatePageTitle = ({ index, count }) => (
  <div className="candidate-page-title">
    {`Showing Candidate ${index + 1} / ${count}`}
  </div>
);

CandidatePageTitle.propTypes = {
  index: PropTypes.number.isRequired,
  count: PropTypes.number.isRequired,
};

export default CandidatePageTitle;
