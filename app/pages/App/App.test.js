import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import candidates from '../../data/candidates.json';
import App from '.';

configure({ adapter: new Adapter() });

describe('Candidates Page', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(<App candidates={candidates} />);
  });

  afterEach(() => wrapper.unmount());

  it('should increase candidate index by 1 when clicked', () => {
    // Before clicked
    const candidateIndexBeforeClicked = wrapper.children().prop('candidateIndex');
    const previousPageButtonContainer = wrapper.find('div.page-navigation-button.next').first();
    previousPageButtonContainer.simulate('click');

    // After clicked
    const candidateIndexAfterClicked = wrapper.children().prop('candidateIndex');

    expect(candidateIndexBeforeClicked).toBe(0);
    expect(candidateIndexAfterClicked).toBe(1);
  });

  it('should show the first candidate after the last is shown', () => {
    // If the number of candidates is n, click n times
    candidates.forEach(() => {
      const previousPageButtonContainer = wrapper.find('div.page-navigation-button.next').first();
      previousPageButtonContainer.simulate('click');
    });

    const candidateIndexAfterClicked = wrapper.children().prop('candidateIndex');
    expect(candidateIndexAfterClicked).toBe(0);
  });


  /**
   * @todo Similar tests with Previous page button
   */
});
