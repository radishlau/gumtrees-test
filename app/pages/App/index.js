import React from 'react';
import PropTypes from 'prop-types';
import { compose, withState } from 'recompose';
import candidates from '../../data/candidates.json';

import { Page, Content } from '../../components/templates';
import { PageNavigationButton, CandidatePageTitle } from '../../components/molecules';
import { CandidateCard } from '../../components/organisms';

import './style.scss';

const App = ({ candidateIndex, setCandidateIndex, candidates }) => {
  const candidateCount = candidates.length;
  const showPreviousCandidate = () => setCandidateIndex(candidateIndex === 0 ? candidateCount - 1 : candidateIndex - 1);
  const showNextCandidate = () => setCandidateIndex((candidateIndex + 1) % candidateCount);
  return (
    <Page className="candidates-page">
      <div className="page-navigation-button__container">
        <PageNavigationButton onClick={showPreviousCandidate} />
      </div>
      <Content contentClassName="candidates-page__content">
        <div className="candidate-info">
          <CandidatePageTitle count={candidateCount} index={candidateIndex} />
          <CandidateCard data={candidates[candidateIndex]} />
        </div>
      </Content>
      <div className="page-navigation-button__container">
        <PageNavigationButton next onClick={showNextCandidate} />
      </div>
    </Page>
  );
};

App.defaultProps = {
  candidates,
};

App.propTypes = {
  candidates: PropTypes.array,
  candidateIndex: PropTypes.number,
  setCandidateIndex: PropTypes.func,
};

const enhance = compose(
  withState('candidateIndex', 'setCandidateIndex', 0),
);

export default enhance(App);
